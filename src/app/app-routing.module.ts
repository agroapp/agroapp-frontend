import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { ProductComponent } from './components/product/product.component';
import {ShoppingcartComponent } from './components/shoppingcart/shoppingcart.component';
import { UserComponent } from './components/user/user.component';
import { USER_ROUTES } from './components/user/user.routes';
import { FirstpageComponent } from './components/firstpage/firstpage.component';
import { VistaProductosComponent } from './components/vista-productos/vista-productos.component';
import { LoginComponent } from './components/login/login.component';
import { SinglepageproductComponent } from './components/singlepageproduct/singlepageproduct.component';



const routes: Routes = [
 
  /*{
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'home',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: []
  },*/
  { path: 'home', component: FirstpageComponent },
  { path: 'productByCategory/:idCategory', component: VistaProductosComponent },
  { path: 'product/:idProduct', component: SinglepageproductComponent },
  { path: 'user', component: UserComponent, children: USER_ROUTES },
  { path: 'products', component: ProductComponent },
  { path: 'login', component: LoginComponent  },
  { path: 'shoppingcar', component: ShoppingcartComponent, canActivate: [AuthGuard] },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
