import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductComponent } from './components/product/product.component';
import { NavComponent } from './components/nav/nav.component';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';
import { ErrorInteceptorProvider } from './services/error.interceptor';
import { AlertifyService } from './services/alertify.service';
import { BsDropdownModule, TypeaheadModule } from 'ngx-bootstrap';
import { ProductService } from './services/product.service';
import { HomeComponent } from './components/home/home.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { UploadFileComponent } from './components/shared/upload-file/upload-file.component';
import { FileUploadModule } from 'ng2-file-upload';
import { UserComponent } from './components/user/user.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { EditComponent } from './components/user/edit/edit.component';
import { UserService } from './services/user.service';
import { ShoppingcartComponent } from './components/shoppingcart/shoppingcart.component';

import { FirstpageComponent } from './components/firstpage/firstpage.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuPrincipalComponent } from './components/menu-principal/menu-principal.component';
import { FiltrosProductosComponent } from './components/filtros-productos/filtros-productos.component';
import { VistaProductosComponent } from './components/vista-productos/vista-productos.component';
import { UserproductComponent } from './components/userproduct/userproduct.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { SinglepageproductComponent } from './components/singlepageproduct/singlepageproduct.component';
import { AddProductComponent } from './components/user/add-product/add-product.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    NavComponent,
    LoginComponent,
    HomeComponent,
    TopNavComponent,
    UploadFileComponent,
    UserComponent,
    ProfileComponent,
    EditComponent,
    ShoppingcartComponent,
    FirstpageComponent,
    FooterComponent,
    MenuPrincipalComponent,
    FiltrosProductosComponent,
    VistaProductosComponent,
    UserproductComponent,
    SinglepageproductComponent,
    AddProductComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    FileUploadModule,
    NgbModule,
    BrowserAnimationsModule,
    MaterialModule,
    TypeaheadModule.forRoot()
  ],
  providers: [
    AuthService,
    UserService,
    AlertifyService,
    ErrorInteceptorProvider,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
