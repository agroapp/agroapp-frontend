import { HttpHeaders } from '@angular/common/http';

export class Endpoint {

    public baseUrl: string;
    public auth: string;
    public product: string;
    public user: string;
    public category: string;
    public shoppingCar:string;

    public httpOptions = {
        headers: new HttpHeaders({
            Authorization: `Bearer ${localStorage.getItem('token')}`
        })
    };

    constructor() {
        this.baseUrl = 'http://localhost:5000/api';
        this.auth = '/auth/';
        this.product = '/product';
        this.user = '/user';
        this.category = '/category';
        this.shoppingCar = '/shoppingCar';
    }
}
