import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Endpoint } from './endpoint';
import { ProductModel } from '../models/products/productModel';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class ProductService {

    private urls: Endpoint = new Endpoint();

    constructor(private http: HttpClient) { }

    getAllProducts(): Observable<ProductModel[]> {
        return this.http.get<ProductModel[]>(`${this.urls.baseUrl}${this.urls.product}`, this.urls.httpOptions);
    }

    createProduct(product: ProductModel) {
        return this.http.post(`${this.urls.baseUrl}${this.urls.product}`, product, this.urls.httpOptions);
    }

    getProductByIdCategory(idCategory:number): Observable<ProductModel[]>{
        return this.http.get<ProductModel[]>(`${this.urls.baseUrl}${this.urls.product}/GetProducstByCategory/${idCategory}`);
    }

    getProduct(idProduct: number) {
        return this.http.get(`${this.urls.baseUrl}${this.urls.product}/GetProductAsync/${idProduct}`);
    }

    getProductByUser(idUsuario:number): Observable<ProductModel[]>{
        return this.http.get<ProductModel[]>(`${this.urls.baseUrl}${this.urls.product}/GetProductByUser/${idUsuario}`,this.urls.httpOptions);
    }

}
