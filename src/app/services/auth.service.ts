import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginModel } from '../models/login/loginModel';
import { Endpoint } from './endpoint';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private urls: Endpoint = new Endpoint();
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  private usuario: string;


  constructor(private http: HttpClient) { }

  login(model: LoginModel) {
    return this.http
      .post(`${this.urls.baseUrl}${this.urls.auth}login`, model)
      .pipe(
        map((response: any) => {
          const user = response;
          if (user) {
            localStorage.setItem('token', user.token);
          }
        })
      );
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  register(model: LoginModel) {
    return this.http.post(`${this.urls.baseUrl}${this.urls.auth}register`, model);
  }

  validateIfUserExist(username: string) {
    return this.http.get(`${this.urls.baseUrl}${this.urls.auth}validateUser/${username}`);
  }

  obtenerUsuario() {
    const token = localStorage.getItem('token');
    this.decodedToken = this.jwtHelper.decodeToken(token);
    return this.decodedToken.nameid;
  }

  obtenerDatosToken(){
    const token = localStorage.getItem('token');
    this.decodedToken = this.jwtHelper.decodeToken(token);
    return this.decodedToken;
  }


  singOut(){
    localStorage.removeItem('token');
  }


}
