import { Injectable } from '@angular/core';
import { Endpoint } from './endpoint';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user/userModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urls: Endpoint = new Endpoint();

  constructor(private http: HttpClient) { }

  GetUser(id: number): Observable<UserModel> {
    return this.http.get<UserModel>(`${this.urls.baseUrl}${this.urls.user}/${id}`, this.urls.httpOptions);
  }
}
