import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Endpoint } from './endpoint';
import { CategoryModel } from '../models/category/CategoryModel';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    private urls: Endpoint = new Endpoint();

    constructor(private http: HttpClient) { }

    getAllCategory(): Observable<CategoryModel[]> {
        return this.http.get<CategoryModel[]>(`${this.urls.baseUrl}${this.urls.category}`);
    }

}