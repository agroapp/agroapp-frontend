import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Endpoint } from './endpoint';
import { addProductShoppingCar } from '../models/addProductShoppingCar';

@Injectable({
  providedIn: 'root'
})
export class ShoppingService {

  private urls: Endpoint = new Endpoint();

  constructor(private http: HttpClient) { }

  obtenerShoppingCar(username: string ) {
    return this.http.get(`${this.urls.baseUrl}${this.urls.shoppingCar}/${username}`, this.urls.httpOptions);
  }  


  agregarProductoShoppingCar(add:addProductShoppingCar){
    return this.http.post(`${this.urls.baseUrl}${this.urls.shoppingCar}/addShoppingCar`,add, this.urls.httpOptions);
  }

}
