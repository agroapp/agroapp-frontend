import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router, NavigationEnd, RouterModule } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public hideElement = false;

  constructor(private authService: AuthService, private route: Router) {

    this.route.events.subscribe(event => {

      if(event instanceof NavigationEnd){
        if(event.url === '/login'){
          this.hideElement = true;
        }else{
          this.hideElement = false;
        }
      }

    })

   }

  jwtHelper = new JwtHelperService();
  ngOnInit() {
    const token = localStorage.getItem('token');
    if (token) {
      this.authService.decodedToken = this.jwtHelper.decodeToken(token);
    }
  }

}
