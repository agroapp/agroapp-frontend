import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-menu-principal',
  templateUrl: './menu-principal.component.html',
  styleUrls: ['./menu-principal.component.css']
})
export class MenuPrincipalComponent implements OnInit {
  category:any;
  user:any;
  isLogged:boolean = false;

  constructor(
    private _categoryService: CategoryService,
    private _router: Router,
    private authService: AuthService
    ) { }

  ngOnInit() {
    this.cargarCategory();

    this.onCheckUser();

  }

  cargarCategory(){
    console.log('Aqui estoy');
    this._categoryService.getAllCategory().subscribe(resp =>{
      this.category = resp;
    }, error =>{
      console.log(error);
    });
  }

  verProductByCategory(idCategory:number){
    console.log(idCategory);
    this._router.navigate(['/productByCategory',idCategory]);
  }

  signOut(){
    this.authService.singOut();
    this._router.navigate(['/home']);
    location.reload();
  }

  onCheckUser(){
    console.log(this.authService.loggedIn());
    this.isLogged = this.authService.loggedIn();
  }


}
