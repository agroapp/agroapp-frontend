import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ShoppingService } from '../../services/shopping.service';

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent implements OnInit {


  shoppingCars:any;
  products:any;
  billing:any;

  constructor(
    private _authService: AuthService, 
    private _router: Router, 
    private _shoppingService: ShoppingService) { }

  ngOnInit() {

    if(!this._authService.loggedIn()){
      this._router.navigate(['/login']);
    }
    const userId = this._authService.obtenerUsuario();

    this._shoppingService.obtenerShoppingCar(userId)
    .subscribe( (data: any ) => {
      this.shoppingCars = data.shopping.shopingCarDetails;
      this.products = data.product;
      this.billing = data.factura;
    });

  }

}
