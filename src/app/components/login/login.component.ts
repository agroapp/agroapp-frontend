import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../../models/login/loginModel';
import { AuthService } from '../../services/auth.service';
import { RegisterModel } from 'src/app/models/login/registerModel';
import { AlertifyService } from '../../services/alertify.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public model: LoginModel = new LoginModel();
  public registerModel: RegisterModel = new RegisterModel();
  public logged: boolean;
  public register: boolean;
  public userExist: boolean;
  public user: any;

  public errorMessage: string;
  public confirmPasswords: boolean;

  constructor(
    private authService: AuthService,
    private alertiService: AlertifyService,
    private route: Router) {
    this.logged = false;
    this.register = false;
    this.confirmPasswords = false;
    this.userExist = false;
    this.user = this.authService.decodedToken;
  }

  ngOnInit() {
    this.logged = this.authService.loggedIn();
    this.user = this.authService.decodedToken;
  }

  login() {
    this.authService.login(this.model).subscribe(resp => {
      console.log(resp);
      this.user = this.authService.decodedToken;
      this.logged = true;
      this.route.navigate(['/home']);
      //location.reload();
    },
      error => {
        console.log(error);
      });
  }

  enterKeyPressed(event: any) {
    if (event.key === 'Enter') {
      this.login();
    }
  }

  singOut(val: boolean) {
    localStorage.removeItem('token');
    this.logged = val;
    this.errorMessage = '';
  }

  changeRegister() {
    if (this.register) {
      this.register = false;
    } else {
      this.register = true;
    }

    this.model.password = '';
    this.model.username = '';
  }

  validateRegisterPasswords() {
    if (this.registerModel.password !== this.registerModel.confirmPassword) {
      this.confirmPasswords = true;
    } else {
      this.confirmPasswords = false;
    }
  }

  async registerUser() {
    this.model.username = this.registerModel.username;
    this.model.password = this.registerModel.password;
    await this.authService.register(this.model).subscribe(
      resp => {
        this.register = false;
        this.login();
      },
      error => {
        this.alertiService.error(error);
      }
    );
  }

  async validateUser(username: string) {
    await this.authService.validateIfUserExist(username)
      .subscribe(resp => {
        if (resp) {
          this.userExist = true;
        }
      },
        error => {
          this.userExist = false;
        }
      );
  }

  irInicio(){
  }


}
