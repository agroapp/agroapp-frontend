import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-firstpage',
  templateUrl: './firstpage.component.html',
  styleUrls: ['./firstpage.component.css']
})
export class FirstpageComponent implements OnInit {
  category:any;
  constructor(
    private _categoryService: CategoryService,
    private router: Router) { }

  ngOnInit() {
    this.cargarCategory();
  }

  cargarCategory(){
    console.log('Aqui estoy');
    this._categoryService.getAllCategory().subscribe(resp =>{
      this.category = resp;
    }, error =>{
      console.log(error);
    });
  }

  verProductByCategory(idCategory:number){
    console.log(idCategory);
    this.router.navigate(['/productByCategory',idCategory]);
  }

}
