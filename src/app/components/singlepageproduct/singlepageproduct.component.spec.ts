import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglepageproductComponent } from './singlepageproduct.component';

describe('SinglepageproductComponent', () => {
  let component: SinglepageproductComponent;
  let fixture: ComponentFixture<SinglepageproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglepageproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglepageproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
