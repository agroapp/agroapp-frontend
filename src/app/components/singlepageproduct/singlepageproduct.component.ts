import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { AuthService } from 'src/app/services/auth.service';
import { addProductShoppingCar } from '../../models/addProductShoppingCar';
import { ShoppingService } from '../../services/shopping.service';
import { AlertifyService } from 'src/app/services/alertify.service';

@Component({
  selector: 'app-singlepageproduct',
  templateUrl: './singlepageproduct.component.html',
  styleUrls: ['./singlepageproduct.component.css']
})
export class SinglepageproductComponent implements OnInit {

  idProduct:any;
  product:any;
  addShopping: addProductShoppingCar;


  constructor(
    private activatedRoute: ActivatedRoute,
    private _serviceProduct: ProductService,
    private _authService: AuthService,
    private _shoppingService: ShoppingService,
    private _alert: AlertifyService,
    private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      this.idProduct = params['idProduct'];
      this.cargarProducto(this.idProduct);
     });
  }


  cargarProducto(id) {
    this._serviceProduct.getProductByIdCategory(id).subscribe(
      (data) => {
        this.product = data[0];
      }, error => {
        console.log(error);
      });
  }

  agregarCarrito(id:number){
    const idUser = this._authService.obtenerUsuario();

    // tslint:disable-next-line: radix
    this.addShopping = new addProductShoppingCar(id, parseInt(idUser), 1);

    console.log(this.addShopping);

    this._shoppingService.agregarProductoShoppingCar(this.addShopping)
    .subscribe( (data: any ) => {
      this._alert.message( ' Producto agregado al carrito exitosamente! ' );

    }, error => {
      console.log(error);
    });

  }


}
