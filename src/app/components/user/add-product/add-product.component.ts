import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CategoryService } from 'src/app/services/category.service';
import { CategoryModel } from 'src/app/models/category/CategoryModel';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { ProductModel } from 'src/app/models/products/productModel';
import { ProductService } from 'src/app/services/product.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  selected: string;
  product: ProductModel;
  categories: any[];

  constructor(private _categoryService: CategoryService,
       private _productService: ProductService,
       private _authService: AuthService,
       private _router: Router) {

    this.product = new ProductModel();
    this._categoryService.getAllCategory().subscribe((resp: CategoryModel[]) => {
      console.log(resp);
      this.categories = resp
    });
  }

  ngOnInit() {
  }

  onSelect(event: TypeaheadMatch): void {
    this.product.categoryId = event.item.id;
  }

  addProduct() {
    if (!this.validateModel()) {
      return;
    }

    this.product.userId = this._authService.decodedToken.nameid

    this._productService.createProduct(this.product)
      .subscribe(resp => {
        console.log(resp)
          this._router.navigate(['/user/products']);
      },
        error => {
          console.log(error);
        });
  }

  validateModel() {
    if (this.product.name.length > 0 &&
      this.product.description.length > 0 &&
      this.product.categoryId != undefined &&
      this.product.price != undefined &&
      this.product.quantity != undefined) {
      return true;
    }
    return false;
  }

}
