import { Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { EditComponent } from './edit/edit.component';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { HomeComponent } from '../home/home.component';
import { UserproductComponent } from '../userproduct/userproduct.component';
import { AddProductComponent } from './add-product/add-product.component';

export const USER_ROUTES: Routes = [

    { path: 'profile', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'products', component: UserproductComponent, canActivate: [AuthGuard] },
    { path: 'addProduct', component: AddProductComponent, canActivate: [AuthGuard]},
    { path: '**', pathMatch: 'full', redirectTo: 'profile' }
];
