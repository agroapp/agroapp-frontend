import { Component, OnInit, Input } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Endpoint } from 'src/app/services/endpoint';


@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent implements OnInit {

  uploader: FileUploader;
  hasBaseDropZoneOver: boolean;
  response: string;

  @Input() url: any;

  constructor() { }

  ngOnInit() {
    this.uploader = new FileUploader({
      url: this.url,
      authToken: `Bearer ${localStorage.getItem('token')}`,
      method: 'put',
      removeAfterUpload: true
    });

    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };

    this.hasBaseDropZoneOver = false;

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
}
