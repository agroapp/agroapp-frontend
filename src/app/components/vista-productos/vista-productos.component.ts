import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../services/product.service';


@Component({
  selector: 'app-vista-productos',
  templateUrl: './vista-productos.component.html',
  styleUrls: ['./vista-productos.component.css']
})
export class VistaProductosComponent implements OnInit {
  idCategory:number;
  product:any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private _serviceProduct: ProductService,
    private router: Router) {

    this.activatedRoute.params.subscribe( params => {
      this.idCategory = params['idCategory'];
      this.cargarProductos(this.idCategory);
     });


   }

  ngOnInit() {
  }

  cargarProductos(idCategory:number){
    this._serviceProduct.getProductByIdCategory(idCategory).subscribe( (data)=>{
      this.product = data;
      console.log(this.product);
    }, error =>{
      console.log('error');
    });
  }


  verProducto(id){
    
    this.router.navigate(['/product',id]);

  }


}
