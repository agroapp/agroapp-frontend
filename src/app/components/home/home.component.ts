import { Component, OnInit} from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userInfo: any;

  constructor(
    private _authService: AuthService) { }

  ngOnInit() {
    this.userInfo = this._authService.obtenerDatosToken();
    console.log(this.userInfo);
   }

   

}
