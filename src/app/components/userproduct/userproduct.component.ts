import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { ProductModel } from 'src/app/models/products/productModel';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { CategoryService } from '../../services/category.service';
import { AuthService } from 'src/app/services/auth.service';
import { ProductService } from '../../services/product.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-userproduct',
  templateUrl: './userproduct.component.html',
  styleUrls: ['./userproduct.component.css']
})
export class UserproductComponent implements OnInit {

  Categoria:any;
  activarForm:boolean = false;
  idUsuario:any;
  displayedColumns: string[] = ['id', 'name', 'description', 'price'];
  dataSource;any;;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    private _category: CategoryService,
    private _authService: AuthService,
    private _productService: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
    this.obtenerCategoria();
    this.idUsuario = this._authService.obtenerDatosToken().nameid;

    this._productService.getProductByUser(this.idUsuario).subscribe( (data) =>{
      console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
    }, error => {
      if(error === "Unauthorized"){
        this._authService.singOut();
        this.router.navigate(['/login']);
      }
    });

  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  editarProducto(id){
    console.log(id);
  }

  obtenerCategoria(){
    this._category.getAllCategory()
    .subscribe( (data: any) => {
      this.Categoria = data;
    });
  }


}
