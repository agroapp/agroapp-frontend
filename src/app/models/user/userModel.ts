export class UserModel {
    public id: number;
    public userName: string;
    public profilePicture: string;
}
