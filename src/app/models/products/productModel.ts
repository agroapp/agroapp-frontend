export class ProductModel {
    public id: number;
    public name: string;
    public description: string;
    public userId?: number;
    public categoryId?: number;
    public image?: string;
    public price: number;
    public quantity: number;
}