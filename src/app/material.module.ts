import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  MatCardModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatToolbarModule,
  MatProgressSpinnerModule,
  MatChipsModule,
  MatDividerModule,
  MatTableModule,
  MatPaginatorModule,
  MatInputModule,
  MatDialogModule,
  MatSelectModule,
  MatFormFieldModule
} from '@angular/material';

const myModule = [
  MatCardModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatToolbarModule,
  MatProgressSpinnerModule,
  MatChipsModule,
  MatDividerModule,
  MatTableModule,
  MatPaginatorModule,
  MatInputModule,
  MatDialogModule,
  MatSelectModule,
  MatFormFieldModule
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    myModule
  ],
  exports: [myModule]
})
export class MaterialModule { }
